import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logoutUser } from "../../actions/authAction";


class Dashboard extends Component {
    onLogOutClick = e => {
        e.preventDefault()
        this.props.logoutUser()
    }

    render() {
        const {user} = this.props.auth
        return(
            <div style={{height: "72vh"}} className="container valign-wrapper">
                <div className="row">
                    <div className="col s12 center-align">
                        <h4>
                            <b>
                                You are logged in,
                            </b>
                                {/* {user.username.split("")[0]} */}
                        </h4>
                        <button
                            style={{
                                width: "150px",
                                borderRadius: "30px",
                                letterSpacing: "1.5px",
                                marginTop: "1rem"
                            }}
                            className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                            onClick={this.onLogOutClick}
                        >
                            Log Out
                        </button>
                    </div>
                </div>

            </div>
        )
    }
}

Dashboard.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const MapStateToProps = state => ({
    auth: state.auth
})

export default connect(MapStateToProps, {logoutUser})(Dashboard);