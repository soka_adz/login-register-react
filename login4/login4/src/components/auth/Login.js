import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import { loginUser } from '../../actions/authAction'
import classnames from 'classnames';


class Login extends Component {
    constructor(){
        super()
        this.state = {
            email: "",
            password: "",
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            // direct user to dashboard screen after login
            this.props.history.push("/dashboard");
        }
            // return error response if users
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            })
        }
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value} )
    }

    onSubmit = e => {
        e.preventDefault();

        const userData = {
            email: this.state.email,
            password: this.state.password
        }

        this.props.loginUser(userData)

        console.log(userData);
    }

    componentDidMount() {
        // if logged in and user navigates to Login page, should redirect
        if (this.props.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }
    }


    render() {

        const { errors } = this.state

        return (
            <div classnames="container">
                <div classnames="row" style={{ marginTop: "4rem" }}>
                    <div classnames="col s8 offset-s2">
                        <Link
                            to='/'
                            classnames='btn-flat waves-effect'
                        >
                            <i className="material-icons left">keyboard_backspace</i>
                            Back to Home
                        </Link>
                        <div classnames="col s12" style={{ paddingLeft: "11.25px" }}>
                            <h4>
                                <b>Login </b> below
                            </h4>
                            <p classnames="grey-text text-darken-2">Don't have an account?          
                                <Link
                                    to='/register'
                                >
                                    Register
                                </Link>
                            </p>
                        </div>
                        <form noValidate onSubmit={this.onSubmit}>
                            <div classnames="input-field col s12">
                            <input
                            onChange={this.onChange}
                            value={this.state.email}
                            error={errors.email}
                            id= "email"
                            type= "email"
                            classnames={classnames("", { invalid: errors.email })}
                        >
                        </input>
                        <label htmlFor="email">Email</label>
                        <span classnames="red-text">{errors.email}</span>
                        </div>
                            
                        <div classnames="input-field col s12">
                            <input
                                onChange={this.onChange}
                                value={this.state.password}
                                error={errors.password}
                                id= "password"
                                type= "password"
                                classnames={ classnames("", {invalid: errors.password})}
                            />
                        <label htmlFor="password">Password</label>
                        <span classnames="red-text">{errors.password}</span>
                            
                        </div>
                        <div classnames="col s12" style={{ paddingLeft: "11.25px"}}>
                            <button classnames="btn btn-large waves-effect waves-light hoverable blue accent-3"
                                style={{
                                    width: '150px',
                                    borderRadius: '3px',
                                    letterSpacing: "1.5px",
                                    marginTop: "1rem"
                                }}
                                type="submit"
                            >
                                Login
                            </button>
                        </div>
                        </form>

                    </div>


                </div>
            
            </div>
        )
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
})

export default connect(mapStateToProps, { loginUser })(Login);