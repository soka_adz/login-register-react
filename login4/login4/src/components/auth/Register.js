import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authAction';
import classnames from 'classnames';

class Register extends Component {
    constructor(){
        super();
        this.state = {
            username:"",
            email:"",
            password:"",
            password2:"",
            errors:{}
        }

    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            })
        }
    }

    onChange = e =>{
        this.setState({ [e.target.id]: e.target.value })
    }

    // klo form diberi
    onSubmit = e =>{
        e.preventDefault();

        const newUser ={
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            // password: this.state.password2
        }

        this.props.registerUser(newUser,this.props.history);

        console.log(newUser)
    }

    componentDidMount() {
        // if logged in and user navigates to Register page, should redirect
        if (this.props.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }
    }

    render(){
       const {errors} = this.state;
       return(
           <div className="container">
               <div className="row">
                   <div className="col s8 offset-s2">
                       <Link to='/' className="btn-flat waves-effect">
                           <i className="material-icons left">keyboard_backspace</i>
                           Back to Home
                       </Link>
                       <div className="col s12" style={{paddingLeft:"11.25px"}}>
                           <h4>
                               <b>
                                   Register
                                </b>
                                    Below
                           </h4>
                           <p>
                               Already have an account?
                               <Link to="/login"> Login </Link>
                           </p>
                       </div>
                       <form noValidate onSubmit={this.onSubmit}>
                           <div className="input-field col s12">
                               <input
                               onChange={this.onChange}
                               value={this.state.username}
                               errors={errors.username}
                               id="username"
                               type="text"
                               className={classnames("",{invalid: errors.username})}
                               />
                               <label htmlFor="username">Username</label>
                               <span className="red-text">{errors.username}</span>
                           </div>
                           <div className="input-field col s12">
                               <input
                               onChange={this.onChange}
                               value={this.state.email}
                               errors={errors.email}
                               id="email"
                               type="email"
                               className={classnames("",{invalid: errors.email})}
                               />
                               <label htmlFor="email">Email</label>
                               <span className="red-text">{errors.username}</span>
                           </div>
                           <div className="input-field col s12">
                               <input
                               onChange={this.onChange}
                               value={this.state.password}
                               errors={errors.password}
                               id="password"
                               type="password"
                               className={classnames("", {invalid: errors.password})}
                               />
                               <label htmlFor="password">Password</label>
                               <span className="red-text">{errors.password}</span>
                           </div>
                           <div className="input-field col s12">
                               <input
                               onChange={this.onChange}
                               value={this.state.password2}
                               errors={errors.password2}
                               id="password2"
                               type="password"
                               className={classnames("", {invalid: errors.password2})}
                               />
                               <label htmlFor="password2">Password Confirm</label>
                               <span className="red-text">{errors.password2}</span>
                           </div>
                           <div className="col s12" style={{paddingLeft:""}}>
                               <button
                               style={{
                                   width:"150 px",
                                   borderRadius:"3px",
                                   letterSpacing:"1.5px",
                                   marginTop:"1rem"
                               }}
                               type="submit"
                               className="btn btn-large waves-effect waves-light hoverable pink accent-3">
                                   Sign Up
                               </button>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       ) 
    }
}

Register.propTypes = {
    registerUser: propTypes.func.isRequired,
    auth: propTypes.object.isRequired,
    errors: propTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
})

export default connect(mapStateToProps, { registerUser })(withRouter(Register));